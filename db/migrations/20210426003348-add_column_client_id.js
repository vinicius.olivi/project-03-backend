"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      // promisse eh JS
      queryInterface.addColumn("clients", "user_id", Sequelize.INTEGER, {
        allowNull: true,
      }),

      queryInterface.addConstraint("clients", {
        fields: ["user_id"],
        type: "foreign key",
        client_name: "client_user_fk",
        // name: "client_user_fk",
        references: {
          table: "users",
          field: "id",
        },
        onDelete: "cascade", // deletou usuario, deleta tudo vinculado a ele
      }),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeConstraint("clients", "client_user_fk"),
      queryInterface.removeColumn("clients", "user_id"),
    ]);
  },
};
