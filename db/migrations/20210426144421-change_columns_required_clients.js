"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.changeColumn(
        "clients",
        "client_name",
        {
          type: Sequelize.TEXT,
        },
        {
          allowNull: true,
        },
      ),
      queryInterface.changeColumn(
        "clients",
        "client_email",
        {
          type: Sequelize.TEXT,
        },
        {
          allowNull: true,
        },
      ),
      queryInterface.changeColumn(
        "clients",
        "client_address",
        {
          type: Sequelize.TEXT,
        },
        {
          allowNull: true,
        },
      ),
      queryInterface.changeColumn(
        "clients",
        "value",
        {
          type: Sequelize.FLOAT,
        },
        {
          allowNull: true,
        },
      ),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.changeColumn(
        "clients",
        "client_name",
        {
          type: Sequelize.TEXT,
        },
        {
          allowNull: false,
        },
      ),

      queryInterface.changeColumn(
        "clients",
        "client_email",
        {
          type: Sequelize.TEXT,
        },
        {
          allowNull: false,
        },
      ),

      queryInterface.changeColumn(
        "clients",
        "client_address",
        {
          type: Sequelize.TEXT,
        },
        {
          allowNull: false,
        },
      ),
      queryInterface.changeColumn(
        "clients",
        "value",
        {
          type: Sequelize.FLOAT,
        },
        {
          allowNull: false,
        },
      ),
    ]);
  },
};
