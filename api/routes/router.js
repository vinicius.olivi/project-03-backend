//-- Módulo responsável pela gestão das versões de rota

// import da propriedade router do express
const { Router } = require("express");

// import de version e name do package.json
const { version, name } = require("../../package.json");

// import da rota de serviços
const servicesRouteV1 = require("./v1/services-route");

//import rota v2
const serviceRoutesv2 = require("../routes/v2/service");
const userRouteV2 = require("./v2/user");

// exportação do módulo
module.exports = (app) => {
  //healthCheck
  app.get("/", (req, res, next) => {
    res.send({ name, version });
  });

  // -- criando uma instância do Router
  const routerV1 = Router();
  servicesRouteV1(routerV1);
  app.use("/v1", routerV1);

  // router v2
  const routerV2 = Router();
  serviceRoutesv2(routerV2);
  userRouteV2(routerV2);
  app.use("/v2", routerV2);

  // router.route("/").get((req, res) => {
  //   res.send(`Bem-vindo à aplicação ${name}, em sua versão ${version}.`);
  // });

  // ServicesRouteV1(router);

  // app.use("/v1", router);
};
