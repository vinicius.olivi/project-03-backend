// import do controlador de rotas dos serviços
const servicesController = require("../../controllers/services-controller");
const { autorizate, validateDTO } = require("../../utils/middlewares.utils");
const Joi = require("joi");
module.exports = (router) => {
  router
    .route("/services")
    .get(autorizate(), servicesController.serviceList)
    .post(
      autorizate("SERVICE_CREATE"),
      validateDTO("body", {
        name: Joi.string().min(5).required(),
        description: Joi.string().min(10).required(),
        manager: Joi.string().required(),
      }),
      servicesController.serviceCreate,
    );

  router.route("/services/:idservice").get(
    autorizate(), // basta verificar se tem token
    validateDTO("params", {
      idservice: Joi.number().integer().required().messages({
        "any.required": `"id" é um campo obrigatório`,
        "number.base": `"id" deve ser um número`,
        "number.integer": `"id" deve ser um número válido`,
      }),
    }),
    servicesController.serviceListById,
  );

  // #SERVICE UPDATE

  router
    .route("/services/:idservice")
    .put(
      autorizate("UPDATE_SERVICE"),
      validateDTO("params", {
        idservice: Joi.number().integer().required().messages({
          "any.required": `"id" é um campo obrigatório`,
          "number.base": `"id" deve ser um número`,
          "number.integer": `"id" deve ser um número válido`,
        }),
      }),
      validateDTO("body", {
        name: Joi.string().required().messages({
          "any.required": `"nome" é um campo obrigatório`,
        }),
        description: Joi.string().required().messages({
          "any.required": `"description" é um campo obrigatório`,
        }),
        manager: Joi.string().required().messages({
          "any.required": `"manager" é um campo obrigatório`,
        }),
      }),
      servicesController.serviceUpdate,
    )
    .delete(
      autorizate("SERVICE_DELETE"),
      validateDTO("params", {
        idservice: Joi.number().integer().required().messages({
          "any.required": `"idservice" é um campo obrigatório`,
          "number.base": `"idservice" deve ser um número`,
          "number.integer": `"idservice" deve ser um número válido`,
        }),
      }),
      servicesController.serviceDelete,
    );

  router.route("/services/:idservice/client").post(
    autorizate("SIGNUP"),
    validateDTO("params", {
      idservice: Joi.number().integer().required().messages({
        "any.required": `"idservice" é um campo obrigatório`,
        "number.base": `"idservice" deve ser um número`,
        "number.integer": `"idservice" deve ser um número válido`,
      }),
    }),
    servicesController.signupInService,
  );

  router.route("/services/:idservice/client/:idclient").delete(
    autorizate("DELETE_SUBSCRIPTION"),
    validateDTO("params", {
      idservice: Joi.number().integer().required().messages({
        "any.required": `"idservice" é um campo obrigatório`,
        "number.base": `"idservice" deve ser um número`,
        "number.integer": `"idservice" deve ser um número válido`,
      }),
      idclient: Joi.number().integer().required().messages({
        "any.required": `"idclient" é um campo obrigatório`,
        "number.base": `"idclient" deve ser um número`,
        "number.integer": `"idclient" deve ser um número válido`,
      }),
    }),
    servicesController.removeSub,
  );
};
