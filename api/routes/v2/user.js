const userController = require("../../controllers/user-controller");
const { validateDTO, autorizate } = require("../../utils/middlewares.utils");
const Joi = require("joi");
// import do controlador de rotas dos serviços

module.exports = (router) => {
  router.route("/auth").post(
    validateDTO("body", {
      user: Joi.string().required().messages({
        "any.required": `usuario eh obrigatorio`,
        "string.empty": `user nao pode ser vazio`,
      }),
      password: Joi.string().required().messages({
        "any.required": `password eh obrigatorio`,
        "string.empty": `password nao pode ser vazio`,
      }),
    }),
    userController.authenticate,
  );

  //nao passa endpoint de autorizacao por ser uma rota publica
  router
    .route("/client")
    .get(autorizate("SERVICE_LIST"), userController.clientList)
    .post(
      validateDTO("body", {
        name: Joi.string().min(5).max(30).required().messages({
          "any.required": `name eh obrigatorio`,
          "string.empty": `name nao pode ser vazio`,
          "string.min": `name nao pode ter menos que  {#limit} caracteres`,
          "string.max": `name nao pode ter mais que  {#limit} caracteres`,
        }),
        email: Joi.string().email().required().messages({
          "any.required": `email eh obrigatorio`,
          "string.empty": `email nao pode ser vazio`,
          "string.email": `email nao eh valido`,
        }),
        password: Joi.string().required().min(6).max(16).messages({
          "any.required": `password eh obrigatorio`,
          "string.empty": `password nao pode ser vazio`,
          "string.min": `password nao pode ter menos que  {#limit} caracteres`,
          "string.max": `password nao pode ter mais que  {#limit} caracteres`,
        }),
        address: Joi.string().min(5).required().messages({
          "any.required": `address eh obrigatorio`,
          "string.empty": `address nao pode ser vazio`,
          "string.min": `address nao pode ter menos que  {#limit} caracteres`,
        }),
        // VERIFICAR O TYPE
        type: Joi.string().required().messages({
          "any.required": `type eh obrigatorio`,
          "string.empty": `type nao pode ser vazio`,
        }),
        annual_income: Joi.number().min(3).messages({
          "number.min": ` nao pode ter menos que  {#limit} caracteres`,
        }),
      }),
      userController.createClient,
    );

  router
    .route("/client/:id")
    .get(autorizate(), userController.searchClientById)
    .put(
      autorizate("UPDATE_CLIENT"),
      validateDTO("params", {
        id: Joi.number().integer().required().messages({
          "any.required": `id eh obrigatorio`,
          "number.base": `id deve ser um numero`,
          "number.integer": `id deve ser um numero valido`,
        }),
      }),
      validateDTO("body", {
        name: Joi.string().min(5).max(30).required().messages({
          "any.required": `name eh obrigatorio`,
          "string.empty": `name nao pode ser vazio`,
          "string.min": `name nao pode ter menos que  {#limit} caracteres`,
          "string.max": `name nao pode ter mais que  {#limit} caracteres`,
        }),
        email: Joi.string().email().required().messages({
          "any.required": `email eh obrigatorio`,
          "string.empty": `email nao pode ser vazio`,
          "string.email": `email nao eh valido`,
        }),
        // daqui
        address: Joi.string().min(5).required().messages({
          "any.required": `address eh obrigatorio`,
          "string.empty": `address nao pode ser vazio`,
          "string.min": `address nao pode ter menos que  {#limit} caracteres`,
        }),
        annual_income: Joi.number().min(3).messages({
          "number.min": ` nao pode ter menos que  {#limit} caracteres`,
        }),
      }),
      userController.clientUpdate,
    );
  router
    .route("/user")
    .get(autorizate("SERVICE_LIST"), userController.usersList);
};
