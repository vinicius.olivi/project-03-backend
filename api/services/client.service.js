const { clients } = require("../models");

const ifSub = async (clientid) => {
  const resultFromDB = await clients.findOne({
    where: {
      id: clientid,
    },
  });

  return resultFromDB ? true : false;
};

const subBelongsToUser = async (clientId, userId) => {
  const resultFromDB = await clients.findOne({
    where: {
      id: clientId,
    },
  });

  return resultFromDB.user_id === userId ? true : false;
};

const ifUserIsSUb = async (serviceId, userId) => {
  const resultFromDB = await clients.findOne({
    where: {
      service_id: serviceId,
      user_id: userId,
    },
  });

  return resultFromDB ? true : false;
};

const subRemove = async (serviceId) => {
  return clients.destroy({
    where: {
      id: serviceId,
    },
  });
};

module.exports = {
  subRemove,
  ifUserIsSUb,
  ifSub,
  subBelongsToUser,
};
