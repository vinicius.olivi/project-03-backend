const { services, clients, users, Sequelize } = require("../models");

// busca uma lista
const listByUserProfile = async (userType, userId) => {
  const resultFromDB = await services.findAll({
    include: [
      {
        model: clients,
        as: "clients",
      },
    ],
  });

  let result = [];

  if (Number(userType) === 2) {
    //condicao do CLIENT
    result = resultFromDB.map((item) => {
      const { id, name, manager, description, clients } = item;

      const filterResult = clients.filter((itemFilter) => {
        return Number(itemFilter.user_id === Number(userId));
      });

      return {
        id,
        name,
        manager,
        description,
        client_subscribe: filterResult.length > 0 ? true : false,
      };
    });
  } else {
    // condicao do ADMIN
    result = resultFromDB.map((item) => {
      const { id, name, manager, description } = item;
      return {
        id,
        name,
        manager,
        description,
        subscribes: item.clients.length,
      };
    });
  }

  return result;
};

// busca um unico usuario
const searchServiceByUserProfile = async (id, userId, userType) => {
  // pesquisar service na base por ID
  const resultFromDB = await services.findOne({
    where: {
      id: id,
    },
    include: [
      {
        model: clients,
        as: "clients",
        include: [
          {
            model: users,
            as: "user",
          },
        ],
      },
    ],
  });

  // verificar perfil de usuario

  let clientsFiltered = resultFromDB.clients;

  if (Number(userType) === 2) {
    clientsFiltered = clientsFiltered.filter((item) => item.user_id === userId);
  }

  const clientsFinal = clientsFiltered.map((itemClient) => {
    return {
      id: itemClient.id,
      user: {
        id: itemClient.id,
        name: itemClient.user.name,
        email: itemClient.user.email,
        address: itemClient.user.address,
        value: itemClient.value,
        annual_income: itemClient.user.annual_income,
      },
    };
  });
  return {
    id: resultFromDB.id,
    name: resultFromDB.name,
    description: resultFromDB.description,
    manager: resultFromDB.manager,
    clients: clientsFinal,
  };
};

const ifNameExist = async (name) => {
  var resultFromDB = await services.findOne({
    where: {
      // name: name, QUBROU AQUI SEM O SEQUELIZE!
      name: Sequelize.where(
        Sequelize.fn("LOWER", Sequelize.col("name")),
        "LIKE",
        name.toLowerCase(),
      ),
    },
  });

  return resultFromDB ? true : false;
};

const serviceCreate = async (body) => {
  return services.create({
    ...body,
  });
};

const serviceUpdate = async (id, body) => {
  //TODO: transformacao de DTO para model fora do servico
  const model = {
    name: body.name,
    description: body.description,
    manager: body.manager,
  };

  // VER
  return services.update({ ...model }, { where: { id: id } });
};

const deleteService = async (id) => {
  return services.destroy({
    where: {
      id: id,
    },
  });
};

const ifService = async (id) => {
  const resultFromDB = await services.findOne({
    where: {
      id,
    },
  });
  return resultFromDB ? true : false;
};

const addClientSubscription = async (serviceId, userId, data) => {
  console.log(data);
  return clients.create({
    service_id: serviceId, //service_id esta definido no migrations de criacao da
    user_id: userId,
    value: data,
  });
};

module.exports = {
  listByUserProfile,
  searchServiceByUserProfile,
  ifNameExist,
  serviceCreate,
  serviceUpdate,
  deleteService,
  ifService,
  addClientSubscription,
};
