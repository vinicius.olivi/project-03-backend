const md5 = require("md5");
const jwt = require("jsonwebtoken");
const hashSecret = process.env.CRYPTO_KEY;
const { users, clients, services } = require("../models");

const searchByEmail = async (email) => {
  return users.findOne({
    // attributes: ["email", "type", "id"],
    where: {
      email: email,
    },
  });
};

const hashCreate = (password) => {
  return md5(password + hashSecret);
};

const isUser = async (user, password) => {
  const userFromDB = await users.findOne({
    //sempre usar pra buscar no DB evitando createdAt...
    where: {
      email: user,
      // password: password,
      password: hashCreate(password),
    },
  });
  return userFromDB ? true : false;
};

const credentialCreate = async (userEmail) => {
  try {
    const user = await users.findOne({
      where: {
        email: userEmail,
      },
    });
    const { id, name, email, type, address, annual_income } = user;

    const credential = {
      //constroi o userJwt do middelware
      token: jwt.sign({ email: user.email }, process.env.JWT_KEY, {
        expiresIn: `${process.env.JWT_VALID_TIME}ms`,
      }),
      user: {
        id,
        name,
        email,
        type,
        address,
        annual_income,
      },
    };
    return credential;
  } catch (error) {}
};

const ifEmailExist = async (email, id = 0) => {
  //se id = 0 esta incluindo, se != esta alterando
  const result = await searchByEmail(email);

  // verifica para novo cadastro
  if (id === 0) {
    return result ? true : false;
  }

  // validacao quando update
  if (result) {
    if (result.id === id) return false;

    return true;
  } else {
    return false;
  }
};

// metodo de criacao de usuario type cliente
const createClient = async (model) => {
  const modelRegister = {
    name: model.name,
    email: model.email,
    type: model.type,
    address: model.address,
    annual_income: model.annual_income,
    password: hashCreate(model.password),
  };
  return users.create(modelRegister);
};

const clientUpdate = async (id, model) => {
  return users.update(
    {
      name: model.name,
      email: model.email,
      address: model.address,
      annual_income: model.annual_income,
    },
    {
      where: { id: id },
    },
  );
};

// aqui
const searchClientById = async (idClient) => {
  const itemDB = await users.findOne({
    where: {
      id: idClient,
    },
    include: {
      model: clients,
      as: "clients",
      include: {
        model: services,
        as: "service",
      },
    },
  });

  const { id, name, email, address, annual_income } = itemDB;

  const resultSubscribes = itemDB.clients.map((itemInscricao) => {
    const { id, service } = itemInscricao;

    return {
      id,
      service: {
        id: service.id,
        name: service.name,
      },
    };
  });

  return {
    id,
    name,
    email,
    address,
    annual_income,
    subscribes: resultSubscribes.length,
    clients: resultSubscribes,
  };
};

// DAqui
const list = async (type) => {
  let where = {};

  if (type)
    where = {
      type,
    };

  const resultFromDB = await users.findAll({
    where,
    include: [
      {
        model: clients,
        as: "clients",
        include: [
          {
            model: services,
            as: "service",
          },
        ],
      },
    ],
  });

  return resultFromDB;
};

const clientList = async () => {
  const resultFromDB = await list("2");

  return resultFromDB.map((item) => {
    const { id, email, name, type, clients } = item;

    return {
      id,
      name,
      email,
      type,
      clients: clients.reduce((acc, item) => {
        const { id, service } = item;
        const newItem = { id, service: service.name };
        return [...acc, newItem];
      }, []),
    };
  });
};

const listAll = async () => {
  const resultFromDB = await list();

  return resultFromDB.map((item) => {
    const { id, email, name, type, clients } = item;

    return {
      id,
      name,
      email,
      type,
      clients: clients.reduce((acc, item) => {
        const { id, service } = item;
        const newItem = { id, service: service.name };
        return [...acc, newItem];
      }, []),
    };
  });
};

module.exports = {
  isUser,
  credentialCreate,
  ifEmailExist,
  createClient,
  searchByEmail,
  clientUpdate,
  clientList,
  listAll,
  searchClientById,
};
