//middleware de validacao

const Joi = require("joi");
const jwt = require("jsonwebtoken");
const userService = require("../services/user.service");

// const profiles = {
//   1: [
//     //admin
//     "SERVICE_CREATE",
//     "UPDATE_SERVICE",
//   ],
//   2: [
//     //cliente
//     "UPDATE_CLIENT",
//   ],
// };
const profiles = [
  {
    id: "1",
    functions: [
      "SERVICE_LIST",
      "SERVICE_CREATE",
      "UPDATE_SERVICE",
      "SERVICE_DELETE",
    ],
  },
  {
    id: "2",
    functions: ["UPDATE_CLIENT", "SIGNUP", "DELETE_SUBSCRIPTION"],
  },
];

const profileById = (profileId) => {
  const result = profiles.find((item) => Number(item.id) === Number(profileId));
  return result;
};

const detailsCreate = (error) => {
  return error.details.reduce((acc, item) => {
    return [...acc, item.message];
  }, []);
};

exports.validateDTO = (type, params) => {
  return (req, res, next) => {
    try {
      const schema = Joi.object().keys(params);

      const { value, error } = schema.validate(req[type], {
        allowUnknown: false,
      });

      req[type] = value;

      return error
        ? res.status(422).send({
            details: [...detailsCreate(error)],
          })
        : next();
    } catch (error) {}
  };
};

exports.autorizate = (route = "*") => {
  return async (req, res, next) => {
    const { token } = req.headers;

    try {
      if (!token) {
        return res.status(403).send({
          message: "usuario nao autorizado.",
        });
      }
      const userJwt = jwt.verify(token, process.env.JWT_KEY);

      //buscando user
      const user = await userService.searchByEmail(userJwt.email);
      req.user = user; //para poder usar as informacoes do user nos proximos passos

      if (route !== "*") {
        const profile = profileById(user.type);
        if (!profile.functions.includes(route)) {
          return res.status(403).send({
            message: "user nao AUTORIZADO",
          });
        }
      }

      next();
    } catch (error) {
      console.log(`Token Error: ${error}`);

      res.status(401).send({
        message: "usuario nao autenticado",
      });
    }
  };
};
