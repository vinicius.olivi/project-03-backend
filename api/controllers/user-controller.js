// const { restart } = require("nodemon");
const userService = require("../services/user.service");

const authenticate = async (req, res, next) => {
  try {
    const { user, password } = req.body;

    const result = await userService.isUser(user, password);

    if (!result) {
      return res.status(401).send({
        message: "usuario ou senha invalidos",
      });
    }

    var credential = await userService.credentialCreate(user);

    return res.status(200).send(credential);
  } catch (error) {
    res.status(500).send({
      message: "#ERROR!!",
    });
  }
};

const createClient = async (req, res, next) => {
  try {
    const { body } = req;
    // validar se email existe

    if (await userService.ifEmailExist(body.email)) {
      return res.status(400).send({
        message: "email ja cadastrado",
      });
    }

    await userService.createClient(body); // await pq so pode retornar 200 se conseguir cadastrar na base
    const credential = await userService.credentialCreate(body.email);

    return res.status(200).send(credential);
  } catch (error) {
    res.status(500).send({
      message: "#ERROR!!",
    });
  }
};

// atualizacao de clientes
const clientUpdate = async (req, res, next) => {
  const { body, params } = req;

  if (Number(params.id) !== Number(req.user.id))
    return res.status(400).send({
      mensagem: `"operação nao permitida.`,
    });

  const emailValidate = await userService.ifEmailExist(body.email, params.id);

  if (emailValidate) {
    return res.status(400).send({
      message: "email ja cadastrado",
    });
  }

  await userService.clientUpdate(params.id, body);

  return res.status(200).send({
    message: "Alteracao realizada com sucesso",
  });
};

// daqui
const searchClientById = async (req, res, next) => {
  try {
    const { params } = req;

    const user = await userService.searchClientById(params.id);

    return res.status(200).send(user);
  } catch (error) {
    return res.status(500).send({
      mensagem: "internal server error",
    });
  }
};
// aqui

const clientList = async (req, res, next) => {
  const clients = await userService.clientList();

  return res.status(200).send(clients);
};

const usersList = async (req, res, next) => {
  const users = await userService.listAll();

  return res.status(200).send(users);
};

module.exports = {
  authenticate,
  createClient,
  clientUpdate,
  clientList,
  usersList,
  searchClientById,
};
