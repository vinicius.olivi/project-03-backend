const { services, clients } = require("../models/index.js");
const servServices = require("../services/serv.service");
const clientServices = require("../services/client.service");

const getAllServices = async (req, res, next) => {
  const result = await services.findAll({});

  res.status(200).send(
    result.map((item) => {
      return {
        id: item.id,
        name: item.name,
        manager: item.manager,
      };
    }),
  ) || [];
};

const getServiceById = async (req, res) => {
  try {
    const result = await services.findOne({
      where: {
        id: req.params.idservice,
      },
      include: {
        model: clients,
        as: "clients",
      },
    });

    const data = {
      id: result.id,
      name: result.name,
      description: result.description,
      manager: result.manager,
      clients: result.clients,
    };

    res.status(200).send(data);
  } catch (error) {}
};

const addClient = async (req, res) => {
  try {
    const body = req.body;
    const { idservice } = req.params;

    const model = {
      service_id: idservice,
      client_name: body.name,
      client_email: body.email,
      client_address: body.address,
      value: body.value,
    };

    await clients.create(model);

    res
      .status(200)
      .send({ message: `Well done, you've created a service order!` });
  } catch (error) {
    res.status(500).send({ message: "Internal server error!!" });
  }
};

const deleteClient = async (req, res) => {
  try {
    const { idclient } = req.params;

    await clients.destroy({
      where: {
        id: idclient,
      },
    });

    res.status(200).send({ message: "Order successfully deleted" });
  } catch (error) {
    res.status(500).send({ message: "Internal server error!!" });
  }
};

//V2!!!

const serviceList = async (req, res, next) => {
  const result = await servServices.listByUserProfile(
    req.user.type,
    req.user.id,
    // req.user.email,
    // req.user.address,
    // req.user.value,
  );
  return res.status(200).send(result);
};

const serviceListById = async (req, res, next) => {
  try {
    const { params, user } = req;

    const result = await servServices.searchServiceByUserProfile(
      params.idservice,
      user.id,
      user.type,
    );
    return res.status(200).send(result);
  } catch (error) {
    return res.status(500).send({
      message: "Internal server error!!",
    });
  }
};

const serviceCreate = async (req, res, next) => {
  try {
    //body
    const { name } = req.body;

    // validacao de nomes repetidos
    var resultName = await servServices.ifNameExist(name);
    if (resultName) {
      return res.status(400).send({
        message: "Ja existe um servico com o nome informado",
      });
    }
    // criando servce no DB
    await servServices.serviceCreate(req.body);

    res.status(200).send({
      message: "Servico criado com sucesso",
    });
  } catch (error) {
    return res.status(500).send({
      message: "Internal server error!!",
    });
  }
};

const serviceUpdate = async (req, res, next) => {
  try {
    const { idservice } = req.params;

    await servServices.serviceUpdate(idservice, req.body);

    return res.status(200).send({
      message: "Service alterado com sucesso",
    });
  } catch (error) {
    return res.status(500).send({
      message: "Internal server error!!",
    });
  }
};

const serviceDelete = async (req, res, next) => {
  try {
    const { params } = req;
    const validationResult = await servServices.ifService(params.idservice);

    if (!validationResult)
      return res.status(422).send({
        message: "Service informado nao encontrado",
      });

    await servServices.deleteService(params.idservice);

    return res.status(200).send({
      message: "operacao realizada com sucesso",
    });
  } catch (error) {
    return res.status(500).send({ message: "Internal server error!!" });
  }
};

const signupInService = async (req, res, next) => {
  try {
    const { params, user } = req;
    const { value } = req.body;

    const validationResult = await servServices.ifService(params.idservice);

    if (!validationResult)
      return res.status(422).send({
        detalhes: ["service informado não existe"],
      });

    const validationSubscription = await clientServices.ifUserIsSUb(
      params.idservice,
      user.id,
    );

    if (validationSubscription)
      return res.status(422).send({
        detalhes: ["usuário ja inscrito"],
      });

    await servServices.addClientSubscription(params.idservice, user.id, value);

    return res.status(200).send({
      message: "operação realizada com sucesso",
    });
  } catch (error) {
    return res.status(500).send({ message: "Internal server error!!" });
  }
};

//Daqui!
const removeSub = async (req, res, next) => {
  try {
    const { params, user } = req;

    //TODO: validacao se curso exister

    const serviceValidation = await servServices.ifService(params.idservice);

    if (!serviceValidation)
      return res.status(422).send({
        detalhes: ["curso informado não existe"],
      });

    //TODO: validacao se inscricao exister

    const subValidation = await clientServices.ifSub(params.idclient);

    if (!subValidation)
      return res.status(422).send({
        detalhes: ["inscrição informada não existe"],
      });

    //TODO: validacao se inscricao pertence ao user que executa a operacao

    const clientUserValidation = await clientServices.subBelongsToUser(
      params.idclient,
      user.id,
    );
    if (!clientUserValidation)
      return res.status(422).send({
        detalhes: ["operação nao pode ser realizada"],
      });

    //TODO: invocar metodo para remover inscricao
    await clientServices.subRemove(params.idclient);

    return res.status(200).send({
      mensagem: "operação realizada com sucesso.",
    });
  } catch (error) {
    return res.status(500).send({ message: "Internal server error!!" });
  }
};

module.exports = {
  getAllServices,
  getServiceById,
  addClient,
  deleteClient,
  serviceCreate,
  serviceUpdate,
  serviceList,
  serviceListById,
  serviceDelete,
  signupInService,
  removeSub,
};
