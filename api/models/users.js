module.exports = (sequelize, DataTypes) => {
  const users = sequelize.define(
    "users",
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        unique: true,
        allowNull: false,
      },
      name: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
      email: {
        allowNull: false,
        type: DataTypes.TEXT,
      },
      address: {
        allowNull: false,
        type: DataTypes.TEXT,
      },
      annual_income: {
        allowNull: true,
        type: DataTypes.FLOAT,
      },
      type: {
        allowNull: false,
        type: DataTypes.TEXT,
      },
      password: {
        allowNull: false,
        type: DataTypes.TEXT,
      },
    },
    {
      undescored: true,
      paranoid: true,
      timestamps: false,
    },
  );

  users.associate = function (models) {
    users.hasMany(models.clients, {
      foreignKey: "user_id",
      as: "clients",
    });
  };

  return users;
};
